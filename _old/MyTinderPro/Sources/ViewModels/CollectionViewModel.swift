//
//  CollectionViewModel.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 10/02/17.
//  Copyright © 2017 Wagner Sales. All rights reserved.
//

import UIKit
import RealmSwift

//**************************************************************************************************
//
// MARK: - Constants -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Definitions -
//
//**************************************************************************************************
enum CollectionType: Int {
	case likedMe = 1
	case browser
	case iLiked
	case matched
	case iDisLiked
	case blocked
	func title() -> String {
		var title = ""
		switch self {
		case .likedMe:
			title = "Liked Me"
		case .browser:
			title = "Browser"
		case .iLiked:
			title = "I Liked"
		case .iDisLiked:
			title = "I Don't Liked"
		case .matched:
			title = "Matches"
		case .blocked:
			title = "Blocked"
		}
		return title
	}
	func tabbarItem() -> UITabBarItem {
		var imageNormal = ""
		switch self {
			case .likedMe:
				imageNormal = "icn_drop"
			case .browser:
				imageNormal = "icn_all"
			case .iLiked:
				imageNormal = "icn_like"
			case .iDisLiked:
				imageNormal = "icn_like"
			case .matched:
				imageNormal = "icn_star"
			case .blocked:
				imageNormal = "icn_star"
		}
		let normalImage = UIImage(named: imageNormal)
		let selectedImage = UIImage(named: imageNormal+"_selected")
		let barItem = UITabBarItem(title: self.title(), image: normalImage, selectedImage: selectedImage)
		barItem.tag = self.rawValue
		return barItem
	}
	func results() -> Results<GASTinder> {
		var results = GASTinderManager.listBrowser()
		switch self {
			case .likedMe:
				results = GASTinderManager.listLikedMe()
			case .browser:
				results = GASTinderManager.listBrowser()
			case .iLiked:
				results = GASTinderManager.listILiked()
			case .iDisLiked:
				results = GASTinderManager.listIDisLiked()
			case .matched:
				results = GASTinderManager.listMatched()
			case .blocked:
				results = GASTinderManager.listBlocked()
		}
		return results
	}
	func canReload() -> Bool {
		var can = false
		switch self {
		case .likedMe, .browser:
			can = true
		default:
			can = false
		}
		return can
	}
}
//**************************************************************************************************
//
// MARK: - Class -
//
//**************************************************************************************************
class CollectionViewModel: NSObject {
	//**************************************************
	// MARK: - Properties
	//**************************************************
	let type: CollectionType!
	var tinders: Results<GASTinder>
	var title: String {
		return self.type.title()
	}
	//**************************************************
	// MARK: - Constructors
	//**************************************************
	init(type: CollectionType = .browser) {
		self.type = type
		self.tinders = self.type.results()
		if type == .matched {
			GASTinderManager.requestMatchesAndBlockers { _ in }
		}
	}
	//**************************************************
	// MARK: - Public Methods
	//**************************************************
	func forceLoad(_ completion: @escaping CompletionSuccess) {
		if self.type == CollectionType.likedMe {
			GASTinderManager.resetLikedMe()
			GASTinderManager.requestLikedMe { success in
				completion(success)
			}
		} else {
			GASTinderManager.requestTinders { (_, success) in
				completion(success)
			}
		}
	}
	func load(_ completion: @escaping CompletionSuccess) {
		guard self.tinders.count == 0 && self.type.canReload() else { return }
		self.forceLoad(completion)
	}
}
