//
//  PersistenceManager.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 28/02/17.
//  Copyright © 2017 Wagner Sales. All rights reserved.
//

import UIKit
import RealmSwift

//**************************************************************************************************
//
// MARK: - Constants -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Definitions -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Class -
//
//**************************************************************************************************

class PersistenceManager: NSObject {
	//**************************************************
	// MARK: - Properties
	//**************************************************
	static var realm: Realm? {
		do {
			let realm = try Realm()
			return realm
		} catch let error {
			print(error)
		}
		return nil
	}
	//**************************************************
	// MARK: - Public Methods
	//**************************************************
	class func add(_ object: Object) {
		guard let realm = PersistenceManager.realm else {
			return
		}
		realm.add(object, update: true)
	}
	class func add<S: Sequence>(_ objects: S) where S.Iterator.Element: Object {
		guard let realm = PersistenceManager.realm else {
			return
		}
		PersistenceManager.write {
			List(objects).setValue(Date(), forKey: "lastUpdate")
			realm.add(objects, update: true)
		}
	}
	class func write(block: () -> Void) {
		guard let realm = PersistenceManager.realm else {
			return
		}
		realm.beginWrite()
		block()
		do {
			try realm.commitWrite()
		} catch let error {
			print(error.localizedDescription)
		}
	}
	class func objects<T: Object>(objectType: T.Type) -> Results<T> {
		var realm: Realm!
		if let persistence = PersistenceManager.realm {
			realm = persistence
		} else {
			do {
				realm = try Realm()
			} catch let error {
				print(error.localizedDescription)
			}
		}
		return realm.objects(objectType)
	}
}
