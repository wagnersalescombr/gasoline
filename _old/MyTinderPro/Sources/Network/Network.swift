//
//  Network.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 09/02/17.
//  Copyright © 2017 Wagner Sales. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

//**************************************************************************************************
//
// MARK: - Definitions -
//
//**************************************************************************************************
typealias Response = DataResponse<Any>
public enum Services: String {
	case Auth		= "auth"
	case List		= "user/recs"
	case Like		= "like/"
	case DisLike	= "pass/"
	case User		= "user/"
	case Update		= "updates"
	case SuperLike	= "/super"
	// swiftlint:disable:next cyclomatic_complexity
	func getStringUrl(_ tinder: GASTinder? = nil) -> String {
		var stringUrl = kBaseUrl
		switch self {
		case .Auth:
			stringUrl += self.rawValue
		case .List:
			stringUrl += self.rawValue + "?locale=en-BR"
		case .Like:
			guard let tinder = tinder else { return "" }
			stringUrl += self.rawValue + tinder.id + "?content_hash=" + tinder.contentHas
		case .DisLike:
			guard let tinder = tinder else { return "" }
			stringUrl += self.rawValue + tinder.id
		case .SuperLike:
			guard let tinder = tinder else { return "" }
			stringUrl += self.rawValue + tinder.id + "?content_hash=" + tinder.contentHas
//			guard let user = MTPUser.userLogged else { return "" }
//			stringUrl += Services.Like.rawValue + user.id + self.rawValue
		case .Update:
			guard let tinder = tinder else { return "" }
			stringUrl += Services.User.rawValue + tinder.id + "?locale=en-BR"
		default:
			stringUrl = kBaseUrl
		}
		return stringUrl
	}
}
//**************************************************************************************************
//
// MARK: - Class -
//
//**************************************************************************************************
class Network: NSObject {
	//**************************************************
	// MARK: - Properties
	//**************************************************
	static let shared = Network()
	var eTag: String?
	var headers: HTTPHeaders {
		var defaultHeaders = HTTPHeaders()
		// Default values
		defaultHeaders["platform"]			= "ios"
		defaultHeaders["Accept-Encoding"]	= "gzip, deflate"
		defaultHeaders["Accept-Language"]	= "en-BR;q=1"
		defaultHeaders["User-Agent"]		= "Tinder/5.0.2 (iPhone; iOS 9.3.1; Scale/2.00)"
		defaultHeaders["Content-Type"]		= "application/json"
		defaultHeaders["os_version"]		= "90000300001"
		defaultHeaders["app-version"]		= "1119"
		defaultHeaders["x-client-version"]	= "50219"
		// eTag if exist
		if let eTag = self.eTag {
			defaultHeaders["If-None-Match"] = eTag
		}
		// Token
		if let user = MTPUser.userLogged, let token = user.tinderToken {
			defaultHeaders["X-Auth-Token"] = token
			defaultHeaders["Authorization"] = String(format: "Token token=\"%@\"", token)
		}
		return defaultHeaders
	}
	//**************************************************
	// MARK: - Private Methods
	//**************************************************
	private func setEtag(response: Response) {
		if let response = response.response {
			let fields = response.allHeaderFields
			if let eTag = fields["Etag"] as? String {
				self.eTag = eTag
			} else if let eTag = fields["ETag"] as? String {
				self.eTag = eTag
			}
		}
	}
	private func statusCode(response: Response) -> Int {
		var statusCode = 0
		if let response = response.response {
			statusCode = response.statusCode
		}
		if statusCode == 401 {
			UIViewController.goToLogin()
		}
		return statusCode
	}
	private func networkActivity(visible: Bool) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = visible
	}
	//**************************************************
	// MARK: - Public Methods
	//**************************************************
	func request(url: String,
	             method: HTTPMethod,
	             parameters: [String: Any]? = nil,
	             completion: @escaping RequestCompletion) {
		self.networkActivity(visible: true)
		let request = Alamofire.request(url,
		                                method: method,
		                                parameters: parameters,
		                                encoding: JSONEncoding.default,
		                                headers: self.headers).response { (_) in
											self.networkActivity(visible: false)
		}
		request.validate(contentType: ["application/json"]).responseJSON { response in
			self.setEtag(response: response)
			let statusCode = self.statusCode(response: response)
			let json = JSON(response.result.value ?? JSON([:]))
			completion(json, response.error, statusCode)
		}
	}
	func requestImage(url: String,
	                  completion: @escaping ((_ image: UIImage?) -> Void)) {
		let request = Alamofire.request(url)
		request.responseImage { (response) in
			completion(response.value)
		}
	}
}
