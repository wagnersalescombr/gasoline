//
//  GASTinderManager.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 10/02/17.
//  Copyright © 2017 Wagner Sales. All rights reserved.
//

import UIKit
import WASKit
import RealmSwift
import SwiftyJSON

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************
let kMaxRequest: Int = 7
//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class GASTinderManager: NSObject {
	//**************************************************
	// MARK: - Properties
	//**************************************************
	static var array = [[GASTinder]]()
	static var requestCount: Int = 0
	//**************************************************
	// MARK: - Constructors
	//**************************************************

	//**************************************************
	// MARK: - Private Methods
	//**************************************************
	class func discoveryMatches() {
		if self.array.count == kMaxRequest {
			
			for array in self.array {
				for tinder in array {
					print(tinder.id)
				}
			}
			
			
			var likedMe = Set(GASTinderManager.listAll())
			for list in self.array {
				likedMe = likedMe.intersection(list)
			}
			PersistenceManager.write {
				List(likedMe).setValue(true, forKey: "wasLiked")
			}
		}
		// clean array
		self.clear()
	}
	class fileprivate func clear() {
		self.requestCount = 0
		self.array = []
	}
	class fileprivate func batchUpdate(tinders: [GASTinder]) {
		guard let tinder = tinders.first, tinder.name.isEmpty else { return }
		var rest = tinders
		GASTinderManager.requestUpdate(tinder: tinder) { (_) in
			rest.removeFirst()
			GASTinderManager.batchUpdate(tinders: rest)
		}
	}
	//**************************************************
	// MARK: - Internal Methods
	//**************************************************

	//**************************************************
	// MARK: - Public Methods
	//**************************************************
	class func findById(id: String) -> GASTinder? {
		guard let realm = PersistenceManager.realm else { return nil }
		let tinder = realm.object(ofType: GASTinder.self, forPrimaryKey: id)
		return tinder
	}
	class func resetLikedMe() {
		PersistenceManager.write {
			GASTinderManager.listLikedMe().setValue(false, forKey: "wasLiked")
		}
	}
}
//**************************************************************************************************
//
// MARK: - Extension - Requests
//
//**************************************************************************************************
extension GASTinderManager {
	class func requestTinders(completion: @escaping (_ tinders: [GASTinder], _ success: Bool) -> Void) {
		let url = Services.List.getStringUrl()
		Network.shared.request(url: url, method: .get) { (json, error, _) in
			if error == nil {
				let tinders = GASTinder.arrayFromJson(json)
				PersistenceManager.add(tinders)
				completion(tinders, true)
			} else {
				completion([], false)
			}
		}
	}
	class func requestLikedMe(completion: @escaping CompletionSuccess) {
		self.requestTinders { (tinders, success) in
			guard success else {
				self.clear()
				completion(false)
				return
			}
			self.requestCount += 1
			self.array.append(tinders)
			if self.requestCount < kMaxRequest {
				self.requestLikedMe(completion: completion)
			} else {
				self.discoveryMatches()
				completion(true)
			}
		}
	}
	class func requestUpdate(tinder: GASTinder, completion: @escaping CompletionSuccess) {
		guard tinder.canUpdate  else { return }
		let url = Services.Update.getStringUrl(tinder)
		Network.shared.request(url: url, method: .get) { (json, error, statusCode) in
			if error == nil {
				if statusCode == 500 {
					let newTinder = GASTinder(value: tinder)
					newTinder.statusCode = statusCode
					PersistenceManager.add([newTinder])
				} else if let tinder = GASTinder(update: json) {
					tinder.lastUpdate = Date()
					tinder.statusCode = statusCode
					PersistenceManager.add([tinder])
				}
				completion(true)
			} else {
				completion(false)
			}
		}
	}
	class func requestMatchesAndBlockers(completion: @escaping CompletionSuccess) {
		let url = String(format: "%@%@", kBaseUrl, kUpdate)
		Network.shared.request(url: url, method: .post, parameters: [:]) { (json, _, statusCode) in
			// Matches
			let matches = GASTinder.arrayFromJsonMatches(json)
			// Blocks
			let blocks = GASTinder.arrayFromJsonBlocks(json)
			let tinders = matches + blocks
			PersistenceManager.add(tinders)
			let blocksFiltered = tinders.filter {
				return $0.statusCode != 500 && $0.name.isEmpty
			}
			GASTinderManager.batchUpdate(tinders: blocksFiltered)
		}
	}
}
//**************************************************************************************************
//
// MARK: - Extension - Lists
//
//**************************************************************************************************
extension GASTinderManager {
	private class var baseFilter: String {
		var filter = "statusCode != 500"
		filter += " AND isBlocked = false"
		return filter
	}
	class func listAll() -> Results<GASTinder> {
		let result = PersistenceManager.objects(objectType: GASTinder.self)
		return result.sorted(byKeyPath: "created", ascending: false)
	}
	class func listLikedMe() -> Results<GASTinder> {
		var filter = self.baseFilter
		filter += " AND isDisLiked = false"
		filter += " AND isLiked = false"
		filter += " AND isMatch = false"
		filter += " AND isSuperLiked = false"
		filter += " AND wasLiked = true"
		let result = GASTinderManager.listAll()
		return result.filter(filter)
	}
	class func listBrowser() -> Results<GASTinder> {
		var filter = self.baseFilter
		filter += " AND isDisLiked = false"
		filter += " AND isLiked = false"
		filter += " AND isMatch = false"
		filter += " AND isSuperLiked = false"
		filter += " AND wasLiked = false"
		filter += " AND wasSuperLiked = false"
		let result = GASTinderManager.listAll()
		return result.filter(filter)
	}
	class func listILiked() -> Results<GASTinder> {
		var filter = self.baseFilter
		filter += " AND isDisLiked = false"
		filter += " AND (isLiked = true OR isSuperLiked = true)"
		filter += " AND isMatch = false"
		filter += " AND wasLiked = false"
		filter += " AND wasSuperLiked = false"
		let result = GASTinderManager.listAll()
		return result.filter(filter)
	}
	class func listIDisLiked() -> Results<GASTinder> {
		var filter = self.baseFilter
		filter += " AND isDisLiked = true"
		let result = GASTinderManager.listAll()
		return result.filter(filter)
	}
	class func listMatched() -> Results<GASTinder> {
		var filter = self.baseFilter
		filter += " AND isMatch = true"
		let result = GASTinderManager.listAll()
		return result.filter(filter).sorted(byKeyPath: "matchLastActivity", ascending: false)
	}
	class func listBlocked() -> Results<GASTinder> {
		var filter = self.baseFilter
		filter += " AND isBlocked = true"
		let result = GASTinderManager.listAll()
		return result.filter(filter)
	}
}
//**************************************************************************************************
//
// MARK: - Extension - Actions
//
//**************************************************************************************************
extension GASTinderManager {
	class func checkMatchAndError(tinder: GASTinder, json: JSON?) {
		var isLiked = false
		var isMatch = false
		if let json = json {
			isLiked = true
			if let _ = json["match"].dictionary {
				let visible = UIViewController.visible
				visible.showAlert(title: "Congratulations", subtitle: "You have a new match")
				isMatch = true
			}
		}
		PersistenceManager.write {
			tinder.isLiked = isLiked
			tinder.isMatch = isMatch
		}
	}
	class func like(tinder: GASTinder) {
		PersistenceManager.write {
			tinder.isLiked = true
		}
		let url = Services.Like.getStringUrl(tinder)
		let network = Network.shared
		network.request(url: url, method: .get) { (json, error, _) in
			let json: JSON? = error == nil ? json : JSON.null
			self.checkMatchAndError(tinder: tinder, json: json)
		}
	}
	class func superLike(tinder: GASTinder) {
		PersistenceManager.write {
			tinder.isLiked = true
		}
		let params = ["content_hash": tinder.contentHas]
		let url = Services.SuperLike.getStringUrl()
		let network = Network.shared
		network.request(url: url, method: .post, parameters: params) { (json, error, _) in
			let json: JSON? = error == nil ? json : JSON.null
			self.checkMatchAndError(tinder: tinder, json: json)
		}
	}
	class func disLike(tinder: GASTinder) {
		PersistenceManager.write {
			tinder.isDisLiked = true
		}
		let url = Services.DisLike.getStringUrl(tinder)
		let network = Network.shared
		network.request(url: url, method: .get) { (_, error, _) in
			guard error != nil else {
				return
			}
			PersistenceManager.write {
				tinder.isDisLiked = false
			}
		}
	}
	class func unmatch(tinder: GASTinder) {
		PersistenceManager.write {
			tinder.isBlocked = true
		}
	}
}

//**************************************************************************************************
//
// MARK: - Extension - Social
//
//**************************************************************************************************
extension GASTinderManager {
	func openInstagram(tinder: GASTinder) {
		guard let instagram = tinder.instagram else {
			return
		}
		let instagramHooks = "instagram://user?username=\(instagram)"
		if let instagramUrl = URL(string: instagramHooks) {
			if UIApplication.shared.canOpenURL(instagramUrl) {
				UIApplication.shared.open(instagramUrl, options: [:], completionHandler: nil)
			} else {
				//redirect to safari because the user doesn't have Instagram
				guard let url = URL(string: "http://instagram.com/\(instagram)") else { return }
				UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
					//
				})
			}
		}
	}
}
