//
//  MTPPhoto.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 5/27/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import Alamofire
import AlamofireImage

//**************************************************************************************************
//
// MARK: - Constants -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Definitions -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Class -
//
//**************************************************************************************************

class GASPhoto: Object {
	//**************************************************
	// MARK: - Properties
	//**************************************************
	dynamic var id = ""
	dynamic var created	= Date(timeIntervalSince1970: 1)
	dynamic var url	= ""
	dynamic var urlBig = ""
	dynamic var urlMid = ""
	dynamic var urlSmall = ""
	dynamic var tinder: GASTinder?
	dynamic var main = false
	//**************************************************
	// MARK: - Constructors
	//**************************************************
	convenience init?(photo: JSON, tinder: GASTinder) {
		let id = photo["id"].stringValue
		guard
			let realm = PersistenceManager.realm,
			!id.isEmpty,
			realm.object(ofType: GASPhoto.self, forPrimaryKey: id) == nil else {
			return nil
		}
		self.init()
		self.id = id
		self.created = Date()
		self.main = photo["main"].boolValue
		self.url = photo["url"].stringValue
		for processed in photo["processedFiles"].arrayValue {
			let width = processed["width"].intValue
			let url	= processed["url"].stringValue
			switch width {
				case 640:
					self.urlBig = url
				case 320:
					self.urlMid = url
				default:
					self.urlSmall = url
			}
		}
		self.tinder = tinder
	}
	//**************************************************
	// MARK: - Override Public Methods
	//**************************************************
	override static func primaryKey() -> String? {
		return "id"
	}
}
