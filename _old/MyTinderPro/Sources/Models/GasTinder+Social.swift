//
//  GasTinder+Social.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 21/10/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import WASKit

extension GASTinder {
	
	var instagram: String? {
		
		var regArray = [String]()

		regArray.append("@")

		regArray.append("insta ")
		regArray.append("insta:")
		regArray.append("insta: ")
		regArray.append("insta : ")
		regArray.append("insta📷:")
		regArray.append("insta📷: ")
		regArray.append("insta📷 : ")
		regArray.append("insta📷 ")
		regArray.append("insta📸:")
		regArray.append("insta📸: ")
		regArray.append("insta📸 : ")
		regArray.append("insta📸 ")

		regArray.append("instagram ")
		regArray.append("instagram:")
		regArray.append("instagram: ")
		regArray.append("instagram : ")
		regArray.append("instagram📷:")
		regArray.append("instagram📷: ")
		regArray.append("instagram📷 : ")
		regArray.append("instagram📷 ")
		regArray.append("instagram📸:")
		regArray.append("instagram📸: ")
		regArray.append("instagram📸 : ")
		regArray.append("instagram📸 ")

		regArray.append("ig ")
		regArray.append("ig:")
		regArray.append("ig: ")
		regArray.append("ig : ")
		regArray.append("ig📷:")
		regArray.append("ig📷: ")
		regArray.append("ig📷 : ")
		regArray.append("ig📷 ")
		regArray.append("ig📸:")
		regArray.append("ig📸: ")
		regArray.append("ig📸 : ")
		regArray.append("ig📸 ")

		regArray.append("📷:")
		regArray.append("📷: ")
		regArray.append("📷 : ")
		regArray.append("📷 ")
		regArray.append("📸:")
		regArray.append("📸: ")
		regArray.append("📸 : ")
		regArray.append("📸 ")

		return self.matchString(regArray)
	}

	var snapchat: String? {

		var regArray = [String]()

		regArray.append("snap ")
		regArray.append("snap:")
		regArray.append("snap: ")
		regArray.append("snap : ")

		regArray.append("snap👻 ")
		regArray.append("snap👻:")
		regArray.append("snap👻: ")
		regArray.append("snap👻 : ")

		regArray.append("snapchat ")
		regArray.append("snapchat:")
		regArray.append("snapchat: ")
		regArray.append("snapchat : ")

		regArray.append("snapchat👻 ")
		regArray.append("snapchat👻:")
		regArray.append("snapchat👻: ")
		regArray.append("snapchat👻 : ")

		regArray.append("👻")
		regArray.append("👻 ")
		regArray.append("👻:")
		regArray.append("👻: ")
		regArray.append("👻 : ")

		return self.matchString(regArray)
	}

	func matchString(_ strings: [String]) -> String? {
		for string in strings {
			if let match = self.bio.start(with: string).first {
				return match
			}
		}
		return nil
	}
}
