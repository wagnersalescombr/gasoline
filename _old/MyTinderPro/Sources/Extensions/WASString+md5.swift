//
//  WASString+md5.swift
//  ListMyPeople
//
//  Created by Wagner Sales on 6/26/16.
//  Copyright © 2016 WAS. All rights reserved.
//

import Foundation

extension String {
	var localized: String {
		return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
	}
	func matchesForRegexInText(_ regexString: String) -> [String] {
		do {
			let regex = try NSRegularExpression(pattern: regexString, options: .caseInsensitive)
			let nsString = self as NSString
			let range = NSMakeRange(0, nsString.length)
			let results = regex.matches(in: self, options: .reportCompletion, range: range)
			return results.map({
				nsString.substring(with: $0.range)
			})
		} catch let e {
			print(e)
		}

		return [String]()
	}

	var md5: String! {
		let str = self.cString(using: String.Encoding.utf8)
		let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
		let digestLen = Int(CC_MD5_DIGEST_LENGTH)
		let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)

		CC_MD5(str!, strLen, result)

		let hash = NSMutableString()
		for i in 0..<digestLen {
			hash.appendFormat("%02x", result[i])
		}

		result.deallocate(capacity: digestLen)

		return String(format: hash as String)
	}
}
