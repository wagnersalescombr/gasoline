//
//  MTPLoginViewController.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 5/24/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import EAIntroView

//**************************************************************************************************
//
// MARK: - Constants -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Definitions -
//
//**************************************************************************************************

//**************************************************************************************************
//
// MARK: - Class -
//
//**************************************************************************************************
class MTPLoginViewController: GASViewController {
	//**************************************************
	// MARK: - Properties
	//**************************************************
	@IBOutlet weak var loginButton: UIButton!
	@IBOutlet weak var introView: UIView!
	var first: Bool = true
	//**************************************************
	// MARK: - Private Methods
	//**************************************************
	private func setupIntro() {
		let page1 = EAIntroPage.init(customViewFromNibNamed: "Page1", bundle: Bundle.main) as Any
		let page2 = EAIntroPage.init(customViewFromNibNamed: "Page2", bundle: Bundle.main) as Any
		let page3 = EAIntroPage.init(customViewFromNibNamed: "Page3", bundle: Bundle.main) as Any
		guard
			let rootView = self.introView,
			let intro = EAIntroView(frame: rootView.frame, andPages: [page1, page2, page3]) else {
				return
		}
		intro.tapToNext = true
		intro.swipeToExit = false
		intro.showSkipButtonOnlyOnLastPage = false
		intro.skipButton = nil
		intro.pageControlY = 10
		intro.show(in: rootView, animateDuration: 0.25)
	}
	//**************************************************
	// MARK: - Self Public Methods
	//**************************************************
	@IBAction func loginButtonTapped(_ sender: AnyObject) {
		AuthManager.login(self) { (success) in
			guard success else {
				self.showError()
				return
			}
			UIViewController.goToMain()
		}
	}
	//**************************************************
	// MARK: - Override Public Methods
	//**************************************************
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if self.first == true {
			self.setupIntro()
			self.first = false
		}
	}
}
