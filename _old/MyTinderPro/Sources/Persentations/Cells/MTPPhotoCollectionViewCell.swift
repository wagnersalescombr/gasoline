//
//  MTPPhotoCollectionViewCell.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 5/25/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

let kMTPPhotoCollectionViewCellIdentifier = "MTPPhotoCollectionViewCellIdentifier"

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class MTPPhotoCollectionViewCell: UICollectionViewCell {

//**************************************************
// MARK: - Properties
//**************************************************

	var photo: GASPhoto!
	var indicator: UIActivityIndicatorView!

	@IBOutlet weak var indicatorView: UIView!
	@IBOutlet weak var photoImageView: UIImageView!

//**************************************************
// MARK: - Constructors
//**************************************************

//**************************************************
// MARK: - Internal Methods
//**************************************************

//**************************************************
// MARK: - Private Methods
//**************************************************

	fileprivate func setupIndicator() {

		if self.indicator == nil {
			self.indicator = UIActivityIndicatorView()
			self.indicator.center = self.indicatorView.center
			self.indicatorView.addSubview(self.indicator)

			self.indicator.translatesAutoresizingMaskIntoConstraints = false
		}
	}

	fileprivate func start() {
		self.indicator.startAnimating()
		UIView.animate(withDuration: 0.25, animations: {
			self.indicatorView.alpha = 0.5
		})
	}

	fileprivate func stop() {
		self.indicator.stopAnimating()
		UIView.animate(withDuration: 0.25, animations: {
			self.indicatorView.alpha = 0
		})
	}

//**************************************************
// MARK: - Self Public Methods
//**************************************************

	func setup(_ photo: GASPhoto) {
		self.start()
		Network.shared.requestImage(url: photo.urlBig) { [weak self] image in
			self?.stop()
			guard let image = image else { return }
			self?.photoImageView.image = image
		}
	}

//**************************************************
// MARK: - Override Public Methods
//**************************************************

	override func prepareForReuse() {
		self.photoImageView.image = nil
	}

	override func awakeFromNib() {
		self.setupIndicator()
	}
}
