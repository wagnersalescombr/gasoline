//
//  GASMessageViewController.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 19/10/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import RealmSwift

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

let kGASMessageCellIdentifier = "GASMessageCellIdentifier"

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class GASMessageViewController: GASViewController {

//**************************************************
// MARK: - Properties
//**************************************************

	var tinder: GASTinder!
	@IBOutlet weak var tableView: UITableView!
//	var messages: List<GASMessage> {
//		return self.tinder.messages
//	}

//**************************************************
// MARK: - Constructors
//**************************************************

//**************************************************
// MARK: - Private Methods
//**************************************************

//**************************************************
// MARK: - Internal Methods
//**************************************************

//**************************************************
// MARK: - Public Methods
//**************************************************

//**************************************************
// MARK: - Override Public Methods
//**************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
		self.tableView.reloadData()
		self.tableView.estimatedRowHeight = 60
		self.setupTitle(self.tinder.name)
    }

}

extension GASMessageViewController: UITableViewDataSource {
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.messages.count
	}
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(kGASMessageCellIdentifier)!
		let message = self.messages[indexPath.row]
		cell.textLabel?.text = message.message

		if message.from == self.tinder.id {
			cell.textLabel?.textAlignment = .Left
		} else {
			cell.textLabel?.textAlignment = .Right
		}
		return cell
	}
}
