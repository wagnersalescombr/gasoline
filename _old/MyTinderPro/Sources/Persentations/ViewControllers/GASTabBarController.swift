//
//  GASTabBarController.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 05/10/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import RealmSwift

class GASTabBarController: UITabBarController {
	//**************************************************
	// MARK: - Private Methods
	//**************************************************
	private func createCollectionViewController(type: CollectionType) -> UINavigationController {
		let viewModel = CollectionViewModel(type: type)
		let viewController = CollectionViewController(viewModel: viewModel)
		viewController.tabBarItem = type.tabbarItem()
		return UINavigationController(rootViewController: viewController)
	}
	private func setupCollectionViews() {
		var viewControllers = [UINavigationController]()
		viewControllers.append(self.createCollectionViewController(type: .likedMe))
		viewControllers.append(self.createCollectionViewController(type: .browser))
		viewControllers.append(self.createCollectionViewController(type: .iLiked))
		viewControllers.append(self.createCollectionViewController(type: .matched))
		self.setViewControllers(viewControllers, animated: false)
	}
	//**************************************************
	// MARK: - Override Public Methods
	//**************************************************
    override func viewDidLoad() {
        super.viewDidLoad()
		self.tabBar.isTranslucent = false
		self.setupCollectionViews()
		let appearance = UITabBarItem.appearance()
		let font = UIFont(name:"Avenir-Book", size: 11) ?? UIFont.systemFont(ofSize: 11)
		appearance.setTitleTextAttributes([NSFontAttributeName: font], for: UIControlState())
    }

	override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
		let itemSelected = tabBar.subviews[item.tag]
		if let view = itemSelected.subviews.flatMap({ $0 as? UIImageView }).first {
			let pulseAnimation = CABasicAnimation(keyPath: "transform.scale")
			pulseAnimation.duration = 0.25
			pulseAnimation.fromValue = 1
			pulseAnimation.toValue = 1.30
			pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
			pulseAnimation.autoreverses = true
			pulseAnimation.repeatCount = 0
			view.layer.add(pulseAnimation, forKey: "animate.transform.scale.\(item.tag)")
		}
	}
}
