//
//  TinderViewController.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 5/25/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import SCLAlertView
import SKPhotoBrowser
import Alamofire
import AlamofireImage
import RealmSwift

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

let kMTPUserViewControllerIdentifier = "MTPUserViewControllerIdentifier"
let kMTPPhotoZoomSegue = "PhotoZoomSegue"
let kTinderMessagesSegue = "TinderMessagesSegue"

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class TinderViewController: GASViewController {

	//**************************************************
	// MARK: - Properties
	//**************************************************

	var viewModel: TinderViewModel!
	var photoSelected: GASPhoto!

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var onlineLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var matchDateLabel: UILabel!
	@IBOutlet weak var matchLastDateLabel: UILabel!

	// Buttons
	@IBOutlet weak var disLikeView: UIView!
	@IBOutlet weak var disLikeButton: UIButton!

	@IBOutlet weak var superLikeView: UIView!
	@IBOutlet weak var superLikeButton: UIButton!

	@IBOutlet weak var likeView: UIView!
	@IBOutlet weak var likeButton: UIButton!

	@IBOutlet weak var dislikeBotton: NSLayoutConstraint!
	@IBOutlet weak var superLikeBotton: NSLayoutConstraint!
	@IBOutlet weak var likeBotton: NSLayoutConstraint!

	//**************************************************
	// MARK: - Constructors
	//**************************************************

	//**************************************************
	// MARK: - Private Methods
	//**************************************************

	//**************************************************
	// MARK: - Self Public Methods
	//**************************************************

	func moreButtonTapped() {

//		let sheet = UIAlertController(title: nil, message: "Escolha a opção", preferredStyle: .actionSheet)
//		
//		if let instagram = self.viewModel.getInsta() {
//			let title = String(format: "Instagram")
//			sheet.addAction(UIAlertAction(title: title, style: .default, handler: {
//				(alert: UIAlertAction!) -> Void in
//				let instagramHooks = String(format: "instagram://user?username=%@", insta)
//				if let instagramUrl = URL(string: instagramHooks) {
//					if UIApplication.shared.canOpenURL(instagramUrl) {
//						UIApplication.shared.openURL(instagramUrl)
//					} else {
//						//redirect to safari because the user doesn't have Instagram
//						let urlString = String(format: "http://instagram.com/%@", insta)
//						UIApplication.shared.openURL(URL(string: urlString)!)
//					}
//				}
//			}))
//		}
//
//		if let snap = self.tinder.getSnap() {
//			let title = String(format: "snapchat")
//			sheet.addAction(UIAlertAction(title: title, style: .default, handler: {
//				(alert: UIAlertAction!) -> Void in
//				let snapHooks = String(format: "snapchat://add/%@", snap)
//				if let snapUrl = URL(string: snapHooks) {
//					if UIApplication.shared.canOpenURL(snapUrl) {
//						UIApplication.shared.openURL(snapUrl)
//					}
//				}
//			}))
//		}
//		
//		sheet.addAction(UIAlertAction(title: "Messages", style: .default, handler: {
//			(alert: UIAlertAction!) -> Void in
//			self.performSegue(withIdentifier: kTinderMessagesSegue, sender: self)
//		}))
//		
//		sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
//			(alert: UIAlertAction!) -> Void in
//		}))
//		
//		self.present(sheet, animated: true, completion: nil)

	}

	//*************************
	// MARK: IBActions
	//*************************

	@IBAction func superLikeButtonTapped(_ sender: AnyObject) {
		self.viewModel.likeButtonTapped()
		self.pop()
	}

	@IBAction func disLikeButtonTapped(_ sender: AnyObject) {
		self.viewModel.disLikeButtonTapped()
		self.pop()
	}

	@IBAction func likeButtonTapped(_ sender: AnyObject) {
		self.viewModel.superLikeButtonTapped()
		self.pop()
	}

	//*************************
	// MARK: Setups
	//*************************

	private func setupViewModel() {
		self.viewModel.completion = {
			self.setupTinder()
		}
		self.viewModel.load()
	}

	private func setConst(_ constraint: NSLayoutConstraint) {
		constraint.constant = 10
	}

	private func animateButtons() {
		UIView.animate(withDuration: 0.25, animations: {
			self.setConst(self.likeBotton)
			self.setConst(self.dislikeBotton)
			self.setConst(self.superLikeBotton)
			self.likeView.layoutIfNeeded()
			self.disLikeView.layoutIfNeeded()
			self.superLikeView.layoutIfNeeded()
		})
	}

	private func prepareButton(_ view: UIView, button: UIButton, color: UIColor) {
		button.layer.cornerRadius = button.frame.width/2
		button.layer.borderWidth = 1
		button.layer.borderColor = LK.cardBorderColor.cgColor
		button.clipsToBounds = true

		let shadowLayer = CAShapeLayer()
		shadowLayer.path = UIBezierPath(roundedRect: button.frame, cornerRadius: button.frame.width/2).cgPath
		shadowLayer.fillColor = LK.cardBorderColor.cgColor

		shadowLayer.shadowColor = UIColor.black.cgColor
		shadowLayer.shadowOpacity = 0.25
		shadowLayer.shadowRadius = 2
		shadowLayer.shadowPath	= shadowLayer.path
		shadowLayer.shadowOffset = CGSize(width: 0, height: 0)

		view.layer.insertSublayer(shadowLayer, at: 0)
		view.clipsToBounds = true
		view.backgroundColor = UIColor.clear
	}

	private func setupButtons() {
		self.prepareButton(self.disLikeView, button: self.disLikeButton, color: LK.redColor)
		self.prepareButton(self.superLikeView, button: self.superLikeButton, color: LK.blueColor)
		self.prepareButton(self.likeView, button: self.likeButton, color: LK.greenColor)
	}

	private func setupTinder() {

		// name
		self.title = self.viewModel.title

		// ping time
		self.onlineLabel.textColor = UIColor.white
		self.onlineLabel.text = self.viewModel.online

		// distance
		self.distanceLabel.text = self.viewModel.distance

		// bio
		self.descriptionLabel.text = self.viewModel.bio

		// match date
		self.matchDateLabel.text = ""

		// match last activity
		self.matchLastDateLabel.text = ""

		// liked
		if self.viewModel.isLiked {
			self.likeButton.setImage(UIImage(named: "btn_liked_big"), for: UIControlState())
		} else {
			self.likeButton.setImage(UIImage(named: "btn_like_big"), for: UIControlState())
		}

		if !self.viewModel.canAction {
			self.disLikeView.isHidden = true
			self.superLikeView.isHidden = true
			self.likeView.isHidden = true
		}
		self.superLikeView.isHidden = true
		self.collectionView.reloadData()
	}

//**************************************************
// MARK: - Override Public Methods
//**************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
		self.setupViewModel()

		self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, self.disLikeView.frame.height, 0)

		self.likeBotton.constant = -80
		self.dislikeBotton.constant = -80
		self.superLikeBotton.constant = -80
		self.setupButtons()
		self.setupTinder()
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.animateButtons()
	}

	override func setupNavigation() {
		super.setupNavigation()
		// Image
		let image = #imageLiteral(resourceName: "btn_more")
		// Frame
		let width = image.size.width
		let height = image.size.height
		let frame = CGRect(x: 0, y: 0, width: width, height: height)
		// Button
		let button = UIButton(frame: frame)
		button.setImage(image, for: UIControlState())
		button.addTarget(self, action: #selector(self.moreButtonTapped), for: .touchUpInside)
		// Button Item
		let buttonItem = UIBarButtonItem(customView: button)
		self.navigationItem.rightBarButtonItem = buttonItem
	}

}

//**********************************************************************************************************
//
// MARK: - Extension - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewLayoutDelegate
//
//**********************************************************************************************************

extension TinderViewController: UICollectionViewDelegate, UICollectionViewDataSource, SKPhotoBrowserDelegate {

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		let array = self.viewModel.photos
		return array.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

		let identifier = kMTPPhotoCollectionViewCellIdentifier
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier,
		                                                    for: indexPath) as? MTPPhotoCollectionViewCell else {
																return UICollectionViewCell()
		}

		let array = self.viewModel.photos
		let photo = array[indexPath.row]
		cell.setup(photo)

		return cell
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

		let array = self.viewModel.photos
		self.photoSelected = array[indexPath.row]
		guard let cell = collectionView.cellForItem(at: indexPath) as? MTPPhotoCollectionViewCell else {
			return
		}

		// URL pattern snippet
		let originImage = cell.photoImageView.image // some image for baseImage

		var images = [SKPhoto]()

		for photo in array {
			print(photo.urlBig)
			let photoBrowser = SKPhoto.photoWithImageURL(photo.urlBig)
			photoBrowser.shouldCachePhotoURLImage = false
			images.append(photoBrowser)
		}

		SKPhotoBrowserOptions.displayToolbar				= true  // all tool bar will be hidden
		SKPhotoBrowserOptions.displayCounterLabel			= false // counter label will be hidden
		SKPhotoBrowserOptions.displayBackAndForwardButton	= false // back/forward button will be hidden
		SKPhotoBrowserOptions.displayAction					= true	// action button will be hidden
		SKPhotoBrowserOptions.enableSingleTapDismiss		= true  // default false
		SKPhotoBrowserOptions.bounceAnimation				= true  // default false

		let browser = SKPhotoBrowser(originImage: originImage!, photos: images, animatedFromView: cell)
		browser.initializePageIndex(indexPath.row)
		browser.delegate = self

		self.present(browser, animated: true, completion: {})
	}

	func didShowPhotoAtIndex(_ index: Int) {
		let indexPath = IndexPath(item: index, section: 0)
		self.collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
	}

	//*************************
	// MARK: UICollectionViewDelegateFlowLayout
	//*************************

	func collectionView(_ collectionView: UICollectionView,
	                    layout collectionViewLayout: UICollectionViewLayout,
	                    sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
		let width = self.collectionView.frame.width
		let height = self.collectionView.frame.height
		return CGSize(width: width, height: height)
	}
}
