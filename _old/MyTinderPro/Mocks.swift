//
//  Mocks.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 11/02/17.
//  Copyright © 2017 Wagner Sales. All rights reserved.
//

import UIKit
import SwiftyJSON

class Mocks: NSObject {

	var tindersJSON: JSON {
		let bundle = Bundle.main
		let filePath = bundle.path(forResource: "tinders", ofType: "json")
		let data = try? Data(contentsOf: URL(fileURLWithPath: filePath!))
		return JSON(data: data!)
	}

	var matchJSON: JSON {
		let bundle = Bundle.main
		let filePath = bundle.path(forResource: "match", ofType: "json")
		let data = try? Data(contentsOf: URL(fileURLWithPath: filePath!))
		return JSON(data: data!)
	}

	var likeJSON: JSON {
		let bundle = Bundle.main
		let filePath = bundle.path(forResource: "like", ofType: "json")
		let data = try? Data(contentsOf: URL(fileURLWithPath: filePath!))
		return JSON(data: data!)
	}

}
