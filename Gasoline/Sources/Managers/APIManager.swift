//
//  APIManager.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 5/23/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import RealmSwift
import SwiftyJSON

//**********************************************************************************************************
//
// MARK: - Constants -
//
//**********************************************************************************************************

let kBaseUrl	= "https://api.gotinder.com/"
let kAuth		= "auth/"
let kLike		= "like/"
let kDislike	= "pass/"
let kUser		= "user/"
let kMatch		= "matches/"
let kUpdate		= "updates"

//**********************************************************************************************************
//
// MARK: - Definitions -
//
//**********************************************************************************************************

public enum ResponseType: Int {
	case like
	case superLike
	case star
	case match
	case update
	case error
}

public struct APIError {
	var message = ""
	var statusCode: Int?
	init(message msg: String) {
		self.message = msg
	}
	init(message msg: String, statusCode: Int) {
		self.init(message: msg)
		self.statusCode = statusCode
	}
}

//**********************************************************************************************************
//
// MARK: - Class -
//
//**********************************************************************************************************

class APIManager: NSObject {

//**************************************************
// MARK: - Properties
//**************************************************

	static fileprivate (set) var instance: APIManager = APIManager()
	var lastUpdate: Date?

	//*************************
	// MARK: Arrays
	//*************************
	var array = [[GASTinder]]()

	//*************************
	// MARK: Counts
	//*************************
	var sessionNotFound: Int = 0
	var requestCount: Int = 0

	//*************************
	// MARK: Header
	//*************************
	var eTag: String?

//**************************************************
// MARK: - Constructors
//**************************************************

	fileprivate override init() {}

//**************************************************
// MARK: - Private Methods
//**************************************************

	fileprivate func getHeaders() -> [String:String] {
		var defaultHeaders = [String: String]()

		defaultHeaders["platform"]			= "ios"
		defaultHeaders["Accept-Encoding"]	= "gzip, deflate"
		defaultHeaders["Accept-Language"]	= "en-us"
		defaultHeaders["User-Agent"]		= "Tinder/5.0.2 (iPhone; iOS 9.3.1; Scale/2.00)"
		defaultHeaders["Content-Type"]		= "application/json"
		defaultHeaders["os_version"]		= "90000300001"
		defaultHeaders["app-version"]		= "1119"
		defaultHeaders["x-client-version"]	= "50219"

		if let eTag = self.eTag {
			defaultHeaders["If-None-Match"] = eTag
		}

		if let user = MTPUser.userLogged {
			if let token = user.tinderToken {
				defaultHeaders["X-Auth-Token"]	= token
				defaultHeaders["Authorization"] = String(format: "Token token=\"%@\"", token)
			}
		}

		return defaultHeaders
	}

	fileprivate func parse(_ response: HTTPURLResponse?, completion: APIManagerResult) {

//		if self.sessionNotFound > 3 {
//			APIManager.logout()
//		}
//		
//		if let response = response {
//			
//			if let eTag = response.allHeaderFields["Etag"] as? String {
//				self.eTag = eTag
//			} else if let eTag = response.allHeaderFields["ETag"] as? String {
//				self.eTag = eTag
//			}
//			
//			let statusCode = response.statusCode
//			if statusCode == 401 {
//				self.sessionNotFound += 1
//				let error = APIError(message: "Session not found", statusCode: statusCode)
//				completion(nil, nil, error)
//			} else {
//				completion(statusCode as AnyObject?, nil, nil)
//			}
//		} else {
//			let error = APIError(message: "Response error")
//			completion(nil, nil, error)
//		}
	}

	fileprivate func parseResponse(_ response: HTTPURLResponse?, finished: (( _ statusCode: Int ) -> Void)) {

//		if let response = response {
//						
//			if let eTag = response.allHeaderFields["Etag"] as? String {
//				self.eTag = eTag
//			} else if let eTag = response.allHeaderFields["ETag"] as? String {
//				self.eTag = eTag
//			}
//			
//			let statusCode = response.statusCode
//			if statusCode == 401 {
//				APIManager.logout()
//			} else {
//				finished(statusCode)
//			}
//		}
	}

	//*************************
	// MARK: Tracker
	//*************************

	fileprivate func trackerLogin() {
//		if let user = MTPUser.loadUserLogged() {
//			let tracker = GAI.sharedInstance().defaultTracker
//			let event = GAIDictionaryBuilder.createEvent(withCategory: "UX", action: "User Sign In", label: "", value: 0)
//			let build = event?.build() as Dictionary<NSObject, AnyObject>
//			tracker?.set(kGAIUserId, value: user.id)
//			tracker.send(build)
//		}
	}

	fileprivate func trackerListTinders() {
//		let tracker = GAI.sharedInstance().defaultTracker
//		let event = GAIDictionaryBuilder.createEvent(withCategory: "Request", action: "List Tinders", label: "", value: 0)
//		let build = event?.build() as Dictionary<NSObject, AnyObject>
//		tracker.send(build)
	}

	fileprivate func trackerTinders(_ action: String) {
//		let tracker = GAI.sharedInstance().defaultTracker
//		let event = GAIDictionaryBuilder.createEvent(withCategory: "Actions", action: action, label: "", value: 0)
//		let build = event?.build() as Dictionary<NSObject, AnyObject>
//		tracker.send(build)
	}

	//*************************
	// MARK: Alerts
	//*************************
	fileprivate func showError(_ msg: String) {
	}

//**************************************************
// MARK: - Self Public Methods
//**************************************************

	class func logout() {
		APIManager.instance = APIManager()
		FBSDKAccessToken.setCurrent(nil)
		MTPUser.delete()
		UIViewController.goToLogin()
	}

	//*************************
	// MARK: Tinder - Actions
	//*************************

	func unmatchTinder(_ tinder: GASTinder) -> Request? {
//		let url = String(format: "%@%@%@%@", kBaseUrl,kUser,kMatch,tinder.matchId)
//		return Alamofire.request(.DELETE, url, parameters: nil, encoding: .JSON, headers: self.getHeaders())
		return nil
	}

	func sendHi(_ tinder: GASTinder) -> Request? {
//		let url = String(format: "%@%@%@%@", kBaseUrl,kUser,kMatch,tinder.matchId)
//		let params = ["message":"Oie! Td bem?"]
//		return Alamofire.request(.POST, url, parameters: params, encoding: .JSON, headers: self.getHeaders())
		return nil
	}

	//*************************
	// MARK: Tinder - Profile
	//*************************

	func profile(_ discoverable: Bool, ageMin: Int, ageMax: Int, distance: Int) {

//		var params = [String : AnyObject]()
//		params["discoverable"] = discoverable as AnyObject?
//		params["age_filter_max"] = ageMax as AnyObject?
//		params["age_filter_min"] = ageMin as AnyObject?
//		params["distance_filter"] = distance as AnyObject?
//		
//		let url = "https://api.gotinder.com/profile"
//		
//		Alamofire.request(.POST, url, parameters: params, 
//		encoding: .JSON, headers: self.getHeaders()).responseJSON { response in
//
//			self.parseResponse(response.response, finished: { (finished) in
//				
//				if let JSON = response.result.value {
//					
//					let _ = MTPUser(json: JSON)
////					success(success: true)
//				} else {
////					success(success: false)
//				}
//				
//			})
//		}
	}

	func update(_ finished: @escaping ((_ JSON: AnyObject) -> Void)) {
//		let url = String(format: "%@%@", kBaseUrl, kUpdate)
//		let params = [String : AnyObject]()
//		Alamofire.request(.POST, url, 
//		parameters: params, encoding: .JSON, headers: self.getHeaders()).responseJSON { response in
//			self.parseResponse(response.response, finished: { (statusCode) in
//				if let JSON = response.result.value {
//					finished(JSON: JSON)
//				}
//			})
//		}
	}

}
