//
//  TinderViewController.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 5/25/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import SCLAlertView
import SKPhotoBrowser
import Alamofire
import AlamofireImage
import RealmSwift

struct Cell {
	
	private static let perLine: CGFloat = 1
	private static let screenWidth: CGFloat = UIScreen.main.bounds.width - 1
	
	static let margin: CGFloat = 0
	
	static var width: CGFloat {
		let perLine = Cell.perLine
		let size = (Cell.screenWidth-(Cell.margin*(perLine+1)))
		return size / perLine
	}
	
	static var height: CGFloat {
		return Cell.width * 1.60
	}
}

// MARK: - Constants

let kMTPUserViewControllerIdentifier = "MTPUserViewControllerIdentifier"
let kMTPPhotoZoomSegue = "PhotoZoomSegue"
let kTinderMessagesSegue = "TinderMessagesSegue"

class TinderViewController: GASViewController {

	// MARK: - Properties

	var viewModel: TinderViewModel!
	var photoSelected: GASPhoto!

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var onlineLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var matchDateLabel: UILabel!
	@IBOutlet weak var matchLastDateLabel: UILabel!

	// Buttons
	@IBOutlet weak var disLikeView: UIView!
	@IBOutlet weak var disLikeButton: UIButton!

	@IBOutlet weak var superLikeView: UIView!
	@IBOutlet weak var superLikeButton: UIButton!

	@IBOutlet weak var likeView: UIView!
	@IBOutlet weak var likeButton: UIButton!

	// MARK: - Self Public Methods
	
	func openUrl(_ urlString: String) {
		guard let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) else {
			return
		}
		UIApplication.shared.open(url, options: [:], completionHandler: nil)
	}

	@objc func moreButtonTapped() {

		let sheet = UIAlertController(title: nil, message: "Escolha a opção", preferredStyle: .actionSheet)

		if let instagram = self.viewModel.instagram {
			let title = String(format: "Instagram")
			sheet.addAction(UIAlertAction(title: title, style: .default, handler: { (alert: UIAlertAction!) -> Void in
				let instagramHooks = String(format: "instagram://user?username=%@", instagram)
				self.openUrl(instagramHooks)
			}))
		}

		if let snap = self.viewModel.snapchat {
			let title = String(format: "snapchat")
			sheet.addAction(UIAlertAction(title: title, style: .default, handler: { (alert: UIAlertAction!) -> Void in
				let snapHooks = String(format: "snapchat://add/%@", snap)
				self.openUrl(snapHooks)
			}))
		}

		sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
			(alert: UIAlertAction!) -> Void in
		}))
		
		self.present(sheet, animated: true, completion: nil)
	}

	// MARK: IBActions

	@IBAction func superLikeButtonTapped(_ sender: AnyObject) {
		self.viewModel.likeButtonTapped()
		self.pop()
	}

	@IBAction func disLikeButtonTapped(_ sender: AnyObject) {
		self.viewModel.disLikeButtonTapped()
		self.pop()
	}

	@IBAction func likeButtonTapped(_ sender: AnyObject) {
		self.viewModel.superLikeButtonTapped()
		self.pop()
	}

	// MARK: Setups

	private func setupViewModel() {
		self.viewModel.completion = {
			self.setupTinder()
		}
		self.viewModel.load()
	}

	private func prepareButton(_ view: UIView, button: UIButton, color: UIColor) {
		button.layer.cornerRadius = button.frame.width/2
		button.layer.borderWidth = 1
		button.layer.borderColor = LK.cardBorderColor.cgColor
		button.clipsToBounds = true

		let shadowLayer = CAShapeLayer()
		shadowLayer.path = UIBezierPath(roundedRect: button.frame, cornerRadius: button.frame.width/2).cgPath
		shadowLayer.fillColor = LK.cardBorderColor.cgColor

		shadowLayer.shadowColor = UIColor.black.cgColor
		shadowLayer.shadowOpacity = 0.25
		shadowLayer.shadowRadius = 2
		shadowLayer.shadowPath	= shadowLayer.path
		shadowLayer.shadowOffset = CGSize(width: 0, height: 0)

		view.layer.insertSublayer(shadowLayer, at: 0)
		view.clipsToBounds = true
		view.backgroundColor = UIColor.clear
	}

	private func setupButtons() {
		self.prepareButton(self.disLikeView, button: self.disLikeButton, color: LK.redColor)
		self.prepareButton(self.superLikeView, button: self.superLikeButton, color: LK.blueColor)
		self.prepareButton(self.likeView, button: self.likeButton, color: LK.greenColor)
	}

	private func setupTinder() {

		// name
		self.title = self.viewModel.title

		// ping time
		self.onlineLabel.textColor = UIColor.white
		self.onlineLabel.text = self.viewModel.online

		// distance
		self.distanceLabel.text = self.viewModel.distance

		// bio
		self.descriptionLabel.text = self.viewModel.bio

		// match date
		self.matchDateLabel.text = ""

		// match last activity
		self.matchLastDateLabel.text = ""

		// liked
		if self.viewModel.isLiked {
			self.likeButton.setImage(UIImage(named: "btn_liked_big"), for: UIControlState())
		} else {
			self.likeButton.setImage(UIImage(named: "btn_like_big"), for: UIControlState())
		}

		if !self.viewModel.canAction {
			self.disLikeView.isHidden = true
			self.superLikeView.isHidden = true
			self.likeView.isHidden = true
		}
		self.superLikeView.isHidden = true
		self.collectionView.reloadData()
	}

	// MARK: - Override Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
		self.setupViewModel()

		self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, self.disLikeView.frame.height, 0)

		self.setupButtons()
		self.setupTinder()
    }

	override func setupNavigation() {
		super.setupNavigation()
		if self.viewModel.instagram != nil || self.viewModel.snapchat != nil {
			// Image
			let image = #imageLiteral(resourceName: "btn_more")

			// Frame
			let width = image.size.width
			let height = image.size.height
			let frame = CGRect(x: 0, y: 0, width: width, height: height)

			// Button
			let button = UIButton(frame: frame)
			button.setImage(image, for: UIControlState())
			button.addTarget(self, action: #selector(self.moreButtonTapped), for: .touchUpInside)

			// Button Item
			let buttonItem = UIBarButtonItem(customView: button)
			self.navigationItem.rightBarButtonItem = buttonItem
		}
	}
}

// MARK: - UICollectionViewDataSource

extension TinderViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		let array = self.viewModel.photos
		return array.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let identifier = kMTPPhotoCollectionViewCellIdentifier
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier,
															for: indexPath) as? MTPPhotoCollectionViewCell else {
																return UICollectionViewCell()
		}
		
		let array = self.viewModel.photos
		let photo = array[indexPath.row]
		cell.setup(photo)
		
		return cell
	}
	
}

// MARK: - UICollectionViewDelegate

extension TinderViewController: UICollectionViewDelegate {

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

		let array = self.viewModel.photos
		self.photoSelected = array[indexPath.row]
		guard let cell = collectionView.cellForItem(at: indexPath) as? MTPPhotoCollectionViewCell else {
			return
		}

		// URL pattern snippet
		let originImage = cell.photoImageView.image // some image for baseImage

		var images = [SKPhoto]()
		for photo in array {
			let photoBrowser = SKPhoto.photoWithImageURL(photo.urlBig)
			photoBrowser.shouldCachePhotoURLImage = false
			images.append(photoBrowser)
		}

		SKPhotoBrowserOptions.displayCounterLabel = false // counter label will be hidden
		SKPhotoBrowserOptions.displayBackAndForwardButton = false // back/forward button will be hidden
		SKPhotoBrowserOptions.displayAction	= true	// action button will be hidden
		SKPhotoBrowserOptions.enableSingleTapDismiss = true  // default false
		SKPhotoBrowserOptions.bounceAnimation = true  // default false

		if let image = originImage {
			let browser = SKPhotoBrowser(originImage: image, photos: images, animatedFromView: cell)
			browser.initializePageIndex(indexPath.row)
			browser.delegate = self
			
			self.present(browser, animated: true, completion: {})
		}
	}
}

// MARK: - UICollectionViewDelegateFlowLayout

extension TinderViewController {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: Cell.width, height: self.collectionView.frame.height)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		let margin = Cell.margin
		return UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return Cell.margin
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return Cell.margin
	}
}

// MARK: - SKPhotoBrowserDelegate

extension TinderViewController: SKPhotoBrowserDelegate {
	func didShowPhotoAtIndex(_ index: Int) {
		let indexPath = IndexPath(item: index, section: 0)
		self.collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
	}
}
