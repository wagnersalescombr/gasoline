//
//  TinderViewModel.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 21/03/17.
//  Copyright © 2017 Wagner Sales. All rights reserved.
//

import UIKit
import RealmSwift

class TinderViewModel: NSObject {

	// MARK: - Properties

	private var tinder: GASTinder!
	var completion: Completion?
	var tinderId: String = ""
	var name: String = ""
	var years: String = ""
	var online: String = ""
	var distance: String = ""
	var bio: String = ""
	var photos: List<GASPhoto> {
		return self.tinder.photos
	}
	var photo: GASPhoto? {
		return self.tinder.photo
	}
	var title: String {
		return self.tinder.fullName
	}
	var canAction: Bool {
		return self.tinder.canAction
	}
	var isLiked: Bool {
		return self.tinder.isLiked
	}
	var instagram: String? {
		return self.tinder.instagram
	}
	var snapchat: String? {
		return self.tinder.snapchat
	}
	
	// MARK: - Constructors

	init(tinder: GASTinder) {
		super.init()
		self.tinder = tinder
		self.setupTinder()
	}

	// MARK: - Private Methods

	private func callCompletion() {
		guard let completion = self.completion else { return }
		completion()
	}
	private func setupTinder() {
		self.tinderId = self.tinder.id
		self.name = self.tinder.name
		self.years = "\(Date().WASyears(from: self.tinder.birthDay))"
		// Online
		var online = ""
		if self.tinder.statusCode == 500 {
			online = "🤕 Deactivated"
		} else {
			online = "" //"Active \(Date().WAStoStringAgo(self.tinder.pingTime)) ago" FIXME:
		}
		self.online = online
		// Distance
		let kms: CGFloat = CGFloat(self.tinder.distance) * 1.609344
		self.distance = String(format: "%.0f km", kms)
		// Bio
		self.bio = self.tinder.bio
		// Completion
		self.callCompletion()
	}

	// MARK: - Public Methods

	func load() {
		GASTinderManager.requestDetails(withTinder: self.tinder) { [weak self] _ in
			self?.setupTinder()
		}
	}
	@objc func likeButtonTapped() {
		if let tinder = GASTinderManager.findById(id: self.tinderId) {
			GASTinderManager.like(tinder: tinder)
		}
	}
	@objc func superLikeButtonTapped() {
		if let tinder = GASTinderManager.findById(id: self.tinderId) {
			GASTinderManager.superLike(tinder: tinder)
		}
	}
	@objc func disLikeButtonTapped() {
		if let tinder = GASTinderManager.findById(id: self.tinderId) {
			GASTinderManager.disLike(tinder: tinder)
		}
	}
}
