//
//  PhotosDataManager.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 1/30/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AsyncDisplayKit

class GASPhotosManager: NSObject {

	static let instance = GASPhotosManager()
	let photoCache = AutoPurgingImageCache(
		memoryCapacity: 100 * 1024 * 1024,
		preferredMemoryUsageAfterPurge: 60 * 1024 * 1024
	)
}

class PhotoImage: NSObject, ASImageContainerProtocol {
	let image: UIImage!
	required init(url: String, image: UIImage) {
		self.image = image
		super.init()
		self.addImageToCache(url, image: image)
	}
	public func asdk_image() -> UIImage? {
		return self.image
	}
	public func asdk_animatedImageData() -> Data? {
		return UIImagePNGRepresentation(self.image)
	}
	private func addImageToCache(_ url: String, image: Image) {
		let photoManager = GASPhotosManager.instance
		photoManager.photoCache.add(image, withIdentifier: url)
	}
	class func loadImageFromCache(_ url: String) -> Image? {
		let photoManager = GASPhotosManager.instance
		return photoManager.photoCache.image(withIdentifier: url)
	}
}

extension GASPhotosManager: ASImageDownloaderProtocol {
	func downloadImage(with URL: URL,
	                   callbackQueue: DispatchQueue,
	                   downloadProgress: AsyncDisplayKit.ASImageDownloaderProgress?,
	                   completion: @escaping AsyncDisplayKit.ASImageDownloaderCompletion) -> Any? {

		Network.shared.requestImage(url: URL.absoluteString) { (image) in
			guard let image = image else {
				completion(nil, nil, nil, nil)
				return
			}
			let photoImage = PhotoImage(url: URL.absoluteString, image: image)
			completion(photoImage, nil, nil, nil)
		}
		return nil
	}
	func cancelImageDownload(forIdentifier downloadIdentifier: Any) {
		if let request = downloadIdentifier as? Request {
			request.cancel()
		}
	}
}

extension GASPhotosManager: ASImageCacheProtocol {
	func cachedImage(with URL: URL,
	                 callbackQueue: DispatchQueue,
	                 completion: @escaping AsyncDisplayKit.ASImageCacherCompletion) {
		guard let image = PhotoImage.loadImageFromCache(URL.absoluteString) else {
			return completion(nil)
		}
		completion(image)
	}
}
