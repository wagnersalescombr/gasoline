//
//  GasTinder+Social.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 21/10/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//

import UIKit
import WASKit

extension GASTinder {
	
	private var instagramArray: [String] {
		var regArray = [String]()
		
		regArray.append("@")
		
		regArray.append("insta ")
		regArray.append("insta:")
		regArray.append("insta: ")
		regArray.append("insta : ")
		regArray.append("insta📷:")
		regArray.append("insta📷: ")
		regArray.append("insta📷 : ")
		regArray.append("insta📷 ")
		regArray.append("insta📸:")
		regArray.append("insta📸: ")
		regArray.append("insta📸 : ")
		regArray.append("insta📸 ")
		
		regArray.append("instagram ")
		regArray.append("instagram:")
		regArray.append("instagram: ")
		regArray.append("instagram : ")
		regArray.append("instagram📷:")
		regArray.append("instagram📷: ")
		regArray.append("instagram📷 : ")
		regArray.append("instagram📷 ")
		regArray.append("instagram📸:")
		regArray.append("instagram📸: ")
		regArray.append("instagram📸 : ")
		regArray.append("instagram📸 ")
		
		regArray.append("ig ")
		regArray.append("ig:")
		regArray.append("ig: ")
		regArray.append("ig : ")
		regArray.append("ig📷:")
		regArray.append("ig📷: ")
		regArray.append("ig📷 : ")
		regArray.append("ig📷 ")
		regArray.append("ig📸:")
		regArray.append("ig📸: ")
		regArray.append("ig📸 : ")
		regArray.append("ig📸 ")
		
		regArray.append("📷:")
		regArray.append("📷: ")
		regArray.append("📷 : ")
		regArray.append("📷 ")
		regArray.append("📸:")
		regArray.append("📸: ")
		regArray.append("📸 : ")
		regArray.append("📸 ")
		
		return regArray
	}
	
	private var snapchatArray: [String] {
		var regArray = [String]()
		
		regArray.append("snap ")
		regArray.append("snap:")
		regArray.append("snap: ")
		regArray.append("snap : ")
		
		regArray.append("snap👻 ")
		regArray.append("snap👻:")
		regArray.append("snap👻: ")
		regArray.append("snap👻 : ")
		
		regArray.append("snapchat ")
		regArray.append("snapchat:")
		regArray.append("snapchat: ")
		regArray.append("snapchat : ")
		
		regArray.append("snapchat👻 ")
		regArray.append("snapchat👻:")
		regArray.append("snapchat👻: ")
		regArray.append("snapchat👻 : ")
		
		regArray.append("👻")
		regArray.append("👻 ")
		regArray.append("👻:")
		regArray.append("👻: ")
		regArray.append("👻 : ")
		
		return regArray
	}
	
	var instagram: String? {
		return self.matchString(self.instagramArray)
	}

	var snapchat: String? {
		return self.matchString(self.snapchatArray)
	}

	private func matchString(_ strings: [String]) -> String? {
		
		let bio = self.bio.lowercased()
		let match: String? = strings.reduce(into: nil) { (result, string) in
			guard let match: String = bio.WASstart(with: string).first else { return }
			result = match.WASremove(string).WAStrimmed
		}
		
		return match
	}
}
