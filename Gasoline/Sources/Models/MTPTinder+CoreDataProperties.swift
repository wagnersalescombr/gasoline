//
//  MTPTinder+CoreDataProperties.swift
//  MyTinderPro
//
//  Created by Wagner Sales on 7/11/16.
//  Copyright © 2016 Wagner Sales. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MTPTinder {

    @NSManaged var bio: String?
    @NSManaged var birthDay: NSDate?
    @NSManaged var created: NSDate?
    @NSManaged var dislikeDate: NSDate?
    @NSManaged var distance: NSNumber?
    @NSManaged var id: String?
    @NSManaged var instagram: String?
    @NSManaged var isBlocked: NSNumber?
    @NSManaged var isDisLiked: NSNumber?
    @NSManaged var isLiked: NSNumber?
    @NSManaged var isMatch: NSNumber?
    @NSManaged var isStared: NSNumber?
    @NSManaged var isSuperLiked: NSNumber?
    @NSManaged var lastActivity: NSDate?
    @NSManaged var likeDate: NSDate?
    @NSManaged var matchDate: NSDate?
    @NSManaged var matchId: String?
    @NSManaged var matchLastActivity: NSDate?
    @NSManaged var name: String?
    @NSManaged var photoUrl: String?
    @NSManaged var pingTime: NSDate?
    @NSManaged var statusCode: NSNumber?
    @NSManaged var wasLiked: NSNumber?
    @NSManaged var wasSuperLiked: NSNumber?
    @NSManaged var snapchat: String?
    @NSManaged var photos: NSSet?

}
